// Bytesbuffer provides a pool of bytes.Buffers.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package bytesbuffer

import (
	"bytes"
	"sync"
)

// pool is a pool of bytes.Buffers ready for use.
var pool = &sync.Pool{
	New: func() interface{} {
		return &bytes.Buffer{}
	},
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new bytes.Buffer.
func New() *bytes.Buffer {
	return pool.Get().(*bytes.Buffer)
}

// NewBytes returns a new bytes.Buffer using buf as its initial contents.
func NewBytes(buf []byte) *bytes.Buffer {
	b := New()
	b.Write(buf)
	return b
}

// NewString returns a new bytes.Buffer using s as its initial contents.
func NewString(s string) *bytes.Buffer {
	b := New()
	b.WriteString(s)
	return b
}

// Reuse returns the given bytes.Buffer to a pool ready for reuse.
func Reuse(b *bytes.Buffer) {
	b.Reset()
	pool.Put(b)
}
